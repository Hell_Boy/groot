import 'dart:html';
import 'package:cms/utils/helper.dart';
import 'package:flutter_web/material.dart';
import 'package:cms/utils/const.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String file_path = SELECT_FILE;
  File _file;
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  double scrHeight;
  double scrWidth;

  @override
  Widget build(BuildContext context) {
    scrHeight = ScreenHeight(context);
    scrWidth = ScreenWidth(context);

    // return Container();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          HOME,
          style: TextStyle(color: Colors.black45),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset('images/groot.png'),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: (scrHeight / 50)),
            child: Image.asset(
              'images/groot_Img.png',
              width: scrWidth / 3,
              height: scrHeight / 4,
            ),
          ),
          _inputField(),
          Container(
              height: scrHeight / 7,
              margin:
                  EdgeInsets.only(left: scrWidth / 2.3, right: scrWidth / 2.3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FloatingActionButton(
                        onPressed: () {ShowSnackbar(_scaffoldKey,MSG_COMING_SOON);},
                        child: Text(CMS),
                        backgroundColor: Colors.red,
                        elevation: 2.0,
                      ),
                    ),
                  ),
                  Expanded( 
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FloatingActionButton(
                        onPressed: () {
                          ShowSnackbar(_scaffoldKey,MSG_COMING_SOON);
                        },
                        child: Text(LMS),
                        elevation: 2.0,
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  Widget _inputField() {
    return Container(
      width: scrWidth / 4,
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(scrWidth / 60)),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: (() {
                    _startFilePicker();
                  }),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text(
                    file_path,
                  )),
              Expanded(
                flex: 1,
                child: IconButton(
                  icon: Icon(Icons.cloud_upload),
                  onPressed: (() {
                    if (_file != null) {
                      sendRequest();
                    } else {
                      ShowSnackbar(_scaffoldKey, FILE_EMPTY);
                      print(FILE_EMPTY);
                    }
                  }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _startFilePicker() async {
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;

      if (files.length == 1) {
        _file = files[0];
        setState(() {
          file_path = _file.name;
        });
      }
    }).onError((e) {
      print('Error : $e');
    });
  }

  void sendRequest() {
    FormData data = FormData();
    // data.append('field', 'some_string');
    data.appendBlob('file', _file);
    HttpRequest.request(
      URL_UPLOAD,
      method: "POST",
      sendData: data,
    ).then((req) {
      print(req.responseText);
      setState(() {
        file_path = SELECT_FILE;
      });
      ShowSnackbar(_scaffoldKey, MSG_UPLOAD_SUCCESSFUL);
    }).catchError((e) {
      ShowSnackbar(_scaffoldKey, MSG_UPLOAD_FAILED);
      print('Error : $e');
    });
  }
}
