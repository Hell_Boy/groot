
import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';


double ScreenWidth(BuildContext context){
  return MediaQuery.of(context).size.width;
}

double ScreenHeight(BuildContext context){
  return MediaQuery.of(context).size.height;
}

ShowSnackbar(GlobalKey<ScaffoldState> key, String msg){
  key.currentState.showSnackBar(SnackBar(content: Text(msg)));
}