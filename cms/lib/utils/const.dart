

// region Strings

// HOST
const String DOMAIN = 'http://192.168.43.10:8000/';
const String URL_UPLOAD = DOMAIN + 'upload/';

// Appbar
const String HOME = 'Groot';

// Buttons
const String BUTTON_UPLOAD = 'Upload';


// Messages
const String MSG_UPLOAD_SUCCESSFUL = 'Uploaded Successfully';
const String MSG_UPLOAD_FAILED = 'Failed to Upload';
const String MSG_COMING_SOON = 'Coming soon';

// Other
const String SELECT_FILE = 'Select File to Upload';
const String FILE_EMPTY = 'Pick a file to proceed';
const String CMS = 'CMS';
const String LMS = 'LMS';


const String HOME_IMAGE = 'https://images.immedia.com.br//27/27250_2_EL.jpg?c=201803290935';
// endregion
